<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Model
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

defined("__AVALID__") or die;

/**
 * Class controller
 *
 * @package     VdProject.frontend
 * @subpackage  Model
 * @since       1.0
 */

class AModel
{
	protected $db;

	/**
	 * Constructor
	 *
	 * @param   string  $model  return to choosen model
	 */
	public function __construct($model = '')
	{
		if (!trim($model))
		{
			$this->db = new ADatabase;
		}
	}

	/**
	 * Get Database Object
	 *
	 * @return ADatabase object
	 */
	public function getDbo()
	{
		return $this->db;
	}

	/**
	 * Insert data for new created project
	 *
	 * @param   int  $proId  Project Id
	 *
	 * @return void 
	 */
	public function initProject($proId)
	{
		$defaultValue = array(
				1 => 5,
				2 => 4,
				3 => 3,
				4 => 2,
				5 => 1
			);

		$defaultSub = array(
				1 => array(
						'Umsatz-Größe',
						'Anzahl Mitarbeiter',
						'Ergebnis (EBIT)',
						'Produkte / Marktanteil',
						'Branche'
					),
				2 => array(
						'Position',
						'Aufgabengebiet',
						'Perspektive',
						'Reportingline',
						'Handlungspielraum'
					),
				3 => array(
						'Standort',
						'Bundesland',
						'Distanz EM',
						'Fahrzeit EM',
						'Lebenshaltungskosten'
					),
				4 => array(
						'Grundgehalt',
						'Variable',
						'Gehalts-Perspektive',
						'Altersvorsorge',
						'Auto'
					),
				5 => array(
						'Chemie zu Kollegen',
						'Motivation für Branche',
						'Erscheinungsbild der Fa.',
						'Innere Motivation für Job',
						'Mein Marktwert in 5 Jahren'
					)
			);

		$table = 'tblMainCategories';
		$columns = array('id', 'catName');
		$mainCats = $this->db->loadObjectList($table, $columns);

		if (count($mainCats))
		{
			foreach ($mainCats as $mc)
			{
				$table = 'tblMainCategoryValue';
				$columns = array(
						'mcid' => $mc->id,
						'pid' => $proId,
						'value' => $defaultValue[$mc->id]
					);

				$this->db->insert($table, $columns);

				foreach ($defaultSub[$mc->id] as $sc)
				{
					$table = 'tblSubCategories';
					$columns = array(
							'pid' => $proId,
							'mcid' => $mc->id,
							'catName' => $sc
						);
					$this->db->insert($table, $columns);
				}
			}
		}
	}

	/**
	 * Function delete a project and related things
	 *
	 * @param   int  $proId  Project Id
	 *
	 * @return void
	 */
	public function deleteProject($proId)
	{
		$table = 'tblProjects';
		$where = array('id' => $proId);
		$this->db->delete($table, $where);
	}

	/**
	 * Function doTask
	 *
	 * @param   object  $item   (type, subtype, id)
	 * @param   mixed   $value  value of item
	 * @param   string  $task   task to do
	 *
	 * @return  void
	 */
	public function doTask($item, $value, $task)
	{
		$table 		= null;
		$columns 	= array();
		$where 		= array();

		switch ($item->type)
		{
			case 'del':
				if ($item->subtype == 'pro')
				{
					if ($task == 'delete')
					{
						$this->deleteProject($item->id);
					}
				}
				break;
			case 'pro':
				if ($item->subtype == 'name')
				{
					$table = 'tblProjects';

					if ($task == 'insert')
					{
						$columns = array(
								'mid' => $item->mid,
								'projectName' => $value,
								'usrDescription' => ''
							);
					}

					if ($task == 'update')
					{
						$columns = array('projectName' => $value);
						$where = array('id' => $item->id);
					}
				}

				if ($item->subtype == 'desc')
				{
					$table = 'tblProjects';

					if ($task == 'insert')
					{
						$columns = array(
								'mid' => $item->mid,
								'projectName' => '',
								'usrDescription' => $value
							);
					}

					if ($task == 'update')
					{
						$columns = array('usrDescription' => $value);
						$where = array('id' => $item->id);
					}
				}
				break;
			case 'mc':
				if ($item->subtype == "value")
				{
					$table = 'tblMainCategoryValue';

					if ($task == 'insert')
					{
						$columns = array(
								'mcid' => $item->id,
								'pid' => $item->pid,
								'value' => $value
							);
					}

					if ($task == 'update')
					{
						$columns = array('value' => $value);
						$where = array(
									'mcid' => $item->id,
									'pid' => $item->pid
									);
					}
				}

				if ($item->subtype == "factor")
				{
					$table = "tblMaincatComXref";

					if ($task == "insert")
					{
						$columns = array(
								'mcid' => $item->id,
								'comid' => $item->cid,
								'factor' => $value,
								'sum' => 0
							);
					}

					if ($task == "update")
					{
						$columns = array(
								'factor' => $value
							);
						$where = array(
								'mcid' => $item->id,
								'comid' => $item->cid
							);
					}
				}

				if ($item->subtype == "sum")
				{
					$table = 'tblMaincatComXref';

					if ($task == "update")
					{
						$columns = array(
								'sum' => $value
							);
						$where = array(
								'mcid' => $item->id,
								'comid' => $item->cid
							);
					}
				}

				break;
			case 'subcat':
				if ($item->subtype == 'title')
				{
					$table 		= 'tblSubCategories';
					$columns 	= array('catName' => $value);
					$where 		= array('id' => $item->id);
				}

				if ($item->subtype == 'desc')
				{
					$table = 'tblSubcatComXref';

					if ($task == 'insert')
					{
						$columns = array(
							'scid' 				=> $item->id,
							'comid' 			=> $item->cid,
							'usrDescription' 	=> $value
							);
					}

					if ($task == 'update')
					{
						$columns = array(
							'usrDescription' 	=> $value
							);
						$where = array(
							'scid' 				=> $item->id,
							'comid' 			=> $item->cid
							);
					}
				}
				break;
			case 'com':
				if ($item->subtype == 'legalform')
				{
					$table 		= 'tblCompanies';

					if ($task == 'insert')
					{
						$columns	= array(
						'pid' => $item->pid,
						'companyName' => '',
						'legalForm' => $value,
						'ranking' => 0,
						'sum' => 0
						);
					}

					if ($task == 'update')
					{
						$columns = array(
							'legalForm' => $value
							);
						$where = array(
							'id' => $item->id
							);
					}
				}

				if ($item->subtype == 'name')
				{
					$table 		= 'tblCompanies';

					if ($task == 'insert')
					{
						$columns	= array(
						'pid' => $item->pid,
						'companyName' => $value,
						'legalForm' => '',
						'ranking' => 0,
						'sum' => 0
						);
					}

					if ($task == 'update')
					{
						$columns = array(
							'companyName' => $value
							);
						$where = array(
							'id' => $item->id
							);
					}
				}

				if ($item->subtype == "sum")
				{
					$table = 'tblCompanies';

					if ($task == "update")
					{
						$columns = array(
								'sum' => $value
							);
						$where = array(
								'id' => $item->id
							);
					}
				}

				if ($item->subtype == "rank")
				{
					$table = 'tblCompanies';

					if ($task == "update")
					{
						$columns = array(
								'ranking' => $value
							);
						$where = array(
								'id' => $item->id
							);
					}
				}
				break;
			default:
				break;
		}

		if ($task == 'update')
		{
			$this->db->update($table, $columns, $where);
		}
		elseif ($task == 'insert')
		{
			$insertedId = $this->db->insert($table, $columns);

			if ($item->type == 'pro')
			{
				$this->initProject($insertedId);
			}

			return $insertedId;
		}
	}
}
