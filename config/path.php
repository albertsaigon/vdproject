<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Entry point
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

defined("__AVALID__") or die;

// ABSOLUTE PATHS
define("ABSOLUTE_ROOT_PATH", '/var/www/techs/vdPROJECT');
define("ABSOLUTE_LIBRARIES_PATH", ABSOLUTE_ROOT_PATH . "/libraries");
define("ABSOLUTE_CONFIG_PATH", ABSOLUTE_ROOT_PATH . "/config");

// HTTP PATHS
define("HTTP_ROOT_PATH", "http://localhost/techs/vdPROJECT");
define("HTTP_CONFIG_PATH", HTTP_ROOT_PATH . "/config");
define("HTTP_LIBRARIES_PATH", HTTP_CONFIG_PATH . "/libraries");
define("HTTP_VIEWS_PATH", HTTP_ROOT_PATH . "/views");
define("HTTP_ASSETS_PATH", HTTP_ROOT_PATH . "/assets");
