<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Entry point
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

defined("__AVALID__") or die;

// Configuration
include_once "path.php";
include_once "lang.php";
include_once "db.php";
include_once "page.php";


// Libraries
include_once "libraries/mpdf/mpdf.php";
include_once 'libraries/libraries.php';
include_once "libraries/core.php";
include_once "libraries/views.php";
include_once "libraries/pdf.php";

// Controller class
include_once __DIR__ . '/../controllers/controller.php';
include_once __DIR__ . '/../models/model.php';
