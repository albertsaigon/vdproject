<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Entry point
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

define("DB_CONNECTION_STRING", "mysql:host=127.0.0.1;dbname=vdproject;charset=utf8");
define("DB_USER", "root");
define("DB_PWD", "root");
define("DB_HOST", "localhost");
define("DB_NAME", "vdproject");
