<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Entry point
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

// Button
define("CREATE_PROJECT_BUTTON", "Create");
define("DELETE_PROJECT_BUTTON", "Delete");

// Connection database
define("PDO_CONNECTION_FAIL", "Connection fail: ");

// Project List
define("APROJECT_NAME", "Project Name");
define("APROJECT_DESCRIPTION", "Description");

// Projects Page
define("APROJECT_DETAIL", "Project Detail");
define("ADELETE", "Delete");
define("APROJECT_FUNCTION", "Function");
