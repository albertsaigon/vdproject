<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Entry point
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

defined("__AVALID__") or die;

/**
 * Clase to connect database and do all query.
 *
 * @package     RedCONTENT.Backend
 * @subpackage  View
 * @since       2.0
 */
class ADatabase
{
	/**
	 * The component title to display in the topbar layout (if using it).
	 * It can be html.
	 *
	 * @var an mysqli connection object
	 */
	protected $db;

	/**
	 * Constructor
	 *
	 */
	public function __construct()
	{
		$this->db = new mysqli(DB_HOST, DB_USER, DB_PWD, DB_NAME);

		// Check connection
		if ($this->db->connect_error)
		{
			trigger_error('Database connection failed: ' . $this->db->connect_error, E_USER_ERROR);
		}

		$this->db->set_charset("utf8");
	}

	/**
	 * Quote Columns Name to put into an sql query
	 * 
	 * @param   mixed  $data  Can be string or array of columns name
	 * 
	 * @return  mixed  string or array
	 */
	protected function quoteName($data)
	{
		$type = gettype($data);

		if ($type == "string")
		{
			$tmpData = explode('.', $data);

			if (count($tmpData))
			{
				foreach ($tmpData as &$item)
				{
					$item = '`' . $item . '`';
				}
			}

			$data = implode('.', $tmpData);
		}

		if ($type == "array")
		{
			if (count($data))
			{
				foreach ($data as &$item)
				{
					$item = $this->quoteName($item);
				}
			}
		}

		return $data;
	}

	/**
	 * Quote value before put it into an sql query
	 *
	 * @param   mixed  $value  mix type variable can be put into sql query
	 * 
	 * @return mixed $value
	 */
	protected function quote($value)
	{
		$type = gettype($value);

		if ($type == "string")
		{
			if (!is_numeric($value))
			{
				$value = "'" . $this->db->escape_string($value) . "'";
			}
			else
			{
				$value = $this->db->escape_string($value);
			}
		}

		if ($type == "array")
		{
			if (count($value))
			{
				foreach ($value as $k => &$v)
				{
					$v = $this->db->escape_string($v);
				}
			}
		}

		return $value;
	}

	/**
	 * Load an array of objects by mysqli
	 *
	 * @param   string  $table      The table
	 * @param   array   $columns    Array of column name
	 * @param   array   $where      Array of where clause
	 * @param   string  $wheretype  Type of where clause
	 * @param   array   $order      Array of order clause
	 * 
	 * @return array of object list
	 */
	public function loadObjectList($table, $columns = array(), $where = array(), $wheretype = "AND", $order = array())
	{
		$result = array();

		$sql = "";

		if (count($columns))
		{
			$columns = $this->quoteName($columns);
			$sql = "SELECT " . implode(', ', $columns);
		}

		if (gettype($table) == "string")
		{
			$sql .= " FROM " . $this->quoteName($table);
		}
		elseif (gettype($table) == "array")
		{
			$firstTable = $this->quoteName(trim($table[0]));
			$secondTable = $this->quoteName(trim($table[1]));
			$joinType = trim($table[2]);
			$onCol1 = $this->quoteName(trim($table[3]));
			$onCol2 = $this->quoteName(trim($table[4]));

			$sql .= " FROM " . $firstTable . " " . $joinType . " " . $secondTable . " ON " . $onCol1 . " = " . $onCol2;
		}

		if (count($where))
		{
			$sql .= " WHERE ";
			$i = 0;

			foreach ($where as $k => $v)
			{
				if ($i++ > 0)
				{
					$sql .= " AND ";
				}

				$sql .= $this->quoteName($k) . " = " . $this->quote($v);
			}
		}

		if (count($order))
		{
			$sql .= " ORDER BY ";

			foreach ($order as $k => $v)
			{
				$sql .= $this->quoteName($k) . " " . $v;
			}
		}

		$rs = $this->db->query($sql);

		if ($rs === false)
		{
			trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $this->db->error, E_USER_ERROR);
			exit;
		}
		else
		{
			while ($row = $rs->fetch_object())
			{
				$result[] = $row;
			}
		}

		return $result;
	}

	/**
	 * Load an array of objects by mysqli
	 *
	 * @param   string  $table      The table
	 * @param   array   $columns    Array of column name
	 * @param   array   $where      Array of where clause
	 * @param   string  $wheretype  Type of where clause
	 * @param   array   $order      Array of order clause
	 * 
	 * @return array of object list
	 */
	public function loadObject($table, $columns = array(), $where = array(), $wheretype = "AND", $order = array())
	{
		$result = new stdClass;

		if (!trim($table))
		{
			return $result;
		}

		$sql = "";

		if (count($columns))
		{
			$columns = $this->quoteName($columns);
			$sql = "SELECT " . implode(', ', $columns);
		}

		$sql .= " FROM " . $this->quoteName($table);

		if (count($where))
		{
			$sql .= " WHERE ";
			$i = 0;

			foreach ($where as $k => $v)
			{
				if ($i++ > 0)
				{
					$sql .= " AND ";
				}

				$sql .= $this->quoteName($k) . " = " . $this->quote($v);
			}
		}

		if (count($order))
		{
			$sql .= " ORDER BY ";

			foreach ($order as $k => $v)
			{
				$sql .= $this->quoteName($k) . " " . $v;
			}
		}

		$rs = $this->db->query($sql);

		if ($rs === false)
		{
			trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $this->db->error, E_USER_ERROR);
			exit;
		}
		else
		{
			if ($rs->num_rows)
			{
				while ($row = $rs->fetch_object())
				{
					$result = $row;
				}
			}
			else
			{
				if (count($columns))
				{
					foreach ($columns as $col)
					{
						$result->$col = '';
					}
				}
			}
		}

		return $result;
	}

	/**
	 * Function update
	 *
	 * @param   mixed   $table      string or array
	 * @param   array   $columns    array of update columns
	 * @param   array   $where      array of where conditions
	 * @param   string  $whereType  AND or OR type
	 *
	 * @return  void
	 */
	public function update($table, $columns, $where, $whereType = 'AND')
	{
		$sql = 'UPDATE ';

		$type = gettype($table);

		if ($type == 'string')
		{
			$sql .= ' ' . $this->quoteName($table);
		}

		$sql .= ' SET ';

		if (count($columns))
		{
			$tmp = array();

			foreach ($columns as $k => $v)
			{
				$tmp[] = $this->quoteName($k) . ' = ' . $this->quote($v);
			}

			$sql .= implode(', ', $tmp);
		}

		if (count($where))
		{
			$sql .= ' WHERE ';
			$tmp = array();

			foreach ($where as $k => $v)
			{
				$tmp[] = $this->quoteName($k) . ' = ' . $this->quote($v);
			}

			$sql .= implode(' ' . $whereType . ' ', $tmp);
		}

		$this->db->query($sql);
	}

	/**
	 * Function insert
	 *
	 * @param   mixed  $table    string or array
	 * @param   array  $columns  array of update columns
	 *
	 * @return  inserted id
	 */
	public function insert($table, $columns)
	{
		$sql = 'INSERT INTO ';

		$type = gettype($table);

		if ($type == 'string')
		{
			$sql .= $this->quoteName($table);
		}

		if (count($columns))
		{
			$insertCol = array();
			$insertVal = array();

			foreach ($columns as $k => $v)
			{
				$insertCol[] = $this->quoteName($k);
				$insertVal[] = $this->quote($v);
			}

			$sql .= '(' . implode(',', $insertCol) . ')';
			$sql .= ' VALUES(' . implode(',', $insertVal) . ')';
		}

		$this->db->query($sql);

		return $this->db->insert_id;
	}

	/**
	 * Function delete
	 *
	 * @param   mixed   $table      table name
	 * @param   array   $where      where conditions
	 * @param   string  $wheretype  default is AND
	 * 
	 * @return void
	 */
	public function delete($table, $where, $wheretype = 'AND')
	{
		$sql = 'DELETE FROM';

		$type = gettype($table);

		if ($type == 'string')
		{
			$sql .= $this->quoteName($table);
		}

		if (count($where))
		{
			$sql .= ' WHERE ';
			$tmp = array();

			foreach ($where as $k => $v)
			{
				$tmp[] = $this->quoteName($k) . ' = ' . $this->quote($v);
			}

			$sql .= implode(' ' . $whereType . ' ', $tmp);
		}

		$this->db->query($sql);
	}
}
