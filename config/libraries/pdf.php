<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Entry point
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

defined("__AVALID__") or die;

/**
 * Clase to create pdf file from HTML content
 *
 * @package     RedCONTENT.Backend
 * @subpackage  View
 * @since       2.0
 */
class APdf extends mPDF
{
	/**
	 * Construct
	 */
	public function __construct()
	{
		parent::__construct();
	}
}
