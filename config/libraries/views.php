<?php
/**
 * @package     VdPROJECT.library
 * @subpackage  Views
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

defined("__AVALID__") or die;

/**
 * Class views system.
 *
 * @package     VdPROJECT.library
 * @subpackage  Views
 * @since       1.0
 */
class AViews
{
	/**
	 * [__construct description]
	 */
	public function __construct()
	{
	}

	/**
	 * Render view function
	 *
	 * @param   object  $obj  View Object
	 * 
	 * @return void
	 */
	public static function render($obj)
	{
		$_SESSION['view_data'] = $obj;

		$path = ABSOLUTE_ROOT_PATH . '/views/' . $obj->view . '/tpl/' . $obj->tpl;

		if (file_exists($path))
		{
			include_once $path;
		}
		else
		{
			echo "Can not found template: " . $path;
		}
	}
}
