<?php
/**
 * @package     VdPROJECT.library
 * @subpackage  core
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

defined("__AVALID__") or die;

/**
 * Class core system.
 *
 * @package     VdPROJECT.library
 * @subpackage  core
 * @since       1.0
 */

class ACore
{
	/**
	 * Constructor
	 */
	public function __construct()
	{
	}

	/**
	 * Create an instance of object
	 *
	 * @param   string  $name  object name
	 * @param   string  $type  object type
	 *
	 * @return  object
	 */
	public static function getInstance($name, $type)
	{
		if ($type === 'views')
		{
			include_once ABSOLUTE_ROOT_PATH . '/views/' . $name . '/' . $name . '.php';
			$name = 'View' . $name;
		}

		return new $name;
	}

	/**
	 * Function get var
	 *
	 * @param   string  $var      var name want to get from $_REQUEST
	 * @param   mixed   $default  default value of $var
	 *
	 * @return  mixed $rs
	 */
	public static function getVar($var, $default)
	{
		$rs = @$_REQUEST["$var"];

		if (!isset($rs))
		{
			$rs = $default;
		}

		return $rs;
	}

	/**
	 * Function get var
	 *
	 * @param   string  $var      var name want to get from $_REQUEST
	 * @param   mixed   $default  default value of $var
	 *
	 * @return  mixed $rs
	 */
	public static function getAssigned($var, $default)
	{
		$rs = @$_SESSION["$var"];

		if (!isset($rs))
		{
			$rs = $default;
		}

		return $rs;
	}
}
