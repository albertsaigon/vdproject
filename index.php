<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Entry point
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

define("__AVALID__", true);

include_once 'config/config.php';

// $mid = ACore::getVar('mid', 0);

$mid = 3;

$_SESSION['vd_mid'] = $mid;

// Load Controller

$view = ACore::getVar('view', DEFAULT_VIEW);

$controller = new AController($view);
