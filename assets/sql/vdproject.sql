-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 31, 2014 at 06:56 AM
-- Server version: 5.5.35-0ubuntu0.13.10.2
-- PHP Version: 5.5.3-1ubuntu2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vdproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `morp_mitarbeiter`
--

CREATE TABLE IF NOT EXISTS `morp_mitarbeiter` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `vorname` varchar(100) NOT NULL,
  `anrede` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `fon` varchar(20) NOT NULL,
  `img1` varchar(100) NOT NULL,
  `img2` varchar(100) NOT NULL,
  `img3` varchar(100) NOT NULL,
  `reihenfolge` int(11) NOT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `morp_mitarbeiter`
--

INSERT INTO `morp_mitarbeiter` (`mid`, `aid`, `name`, `vorname`, `anrede`, `email`, `fon`, `img1`, `img2`, `img3`, `reihenfolge`) VALUES
(3, 0, 'Müller', 'Herbert', 'Sehr geehrter Herr', 'test', 'aa', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblCompanies`
--

CREATE TABLE IF NOT EXISTS `tblCompanies` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Company ID',
  `pid` int(11) NOT NULL COMMENT 'Project ID',
  `companyName` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Company Name',
  `legalForm` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Legal Form',
  `ranking` float NOT NULL DEFAULT '0' COMMENT 'Ranking of Company',
  `sum` float NOT NULL COMMENT 'Sum',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Store information of companies' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblMaincatComXref`
--

CREATE TABLE IF NOT EXISTS `tblMaincatComXref` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `mcid` int(11) NOT NULL COMMENT 'Main Category ID',
  `comid` int(11) NOT NULL COMMENT 'Company ID',
  `factor` float NOT NULL COMMENT 'Factor of each main category',
  `sum` float NOT NULL COMMENT 'sum of each main category',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Store information related between Main Category and Company' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblMainCategories`
--

CREATE TABLE IF NOT EXISTS `tblMainCategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Main category id',
  `mid` int(11) NOT NULL COMMENT 'Refer to user id',
  `catName` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Category name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Store catego' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblMainCategoryValue`
--

CREATE TABLE IF NOT EXISTS `tblMainCategoryValue` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `mcid` int(11) NOT NULL COMMENT 'Main Category ID',
  `pid` int(11) NOT NULL COMMENT 'Project ID',
  `value` float NOT NULL COMMENT 'Value of Main Category belong to a project',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Store value of main category belong to each project' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblProjects`
--

CREATE TABLE IF NOT EXISTS `tblProjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Project ID',
  `mid` int(11) NOT NULL COMMENT 'User ID',
  `projectName` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Project Name',
  `usrDescription` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Project description by user',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Store projects information' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblSubcatComXref`
--

CREATE TABLE IF NOT EXISTS `tblSubcatComXref` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `scid` int(11) NOT NULL COMMENT 'Sub-category ID',
  `comid` int(11) NOT NULL COMMENT 'Company ID',
  `usrDescription` int(11) NOT NULL COMMENT 'Sub-category description by user',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Store information related between Sub Categories and Companies' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblSubCategories`
--

CREATE TABLE IF NOT EXISTS `tblSubCategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Sub-Category ID',
  `pid` int(11) NOT NULL COMMENT 'Project ID',
  `mcid` int(11) NOT NULL COMMENT 'Main Category ID',
  `catName` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Sub-Category Name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Store sub categories information' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
