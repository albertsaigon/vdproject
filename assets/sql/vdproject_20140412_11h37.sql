-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 12, 2014 at 04:36 AM
-- Server version: 5.5.35-0ubuntu0.13.10.2
-- PHP Version: 5.5.3-1ubuntu2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vdproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `morp_mitarbeiter`
--

CREATE TABLE IF NOT EXISTS `morp_mitarbeiter` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `vorname` varchar(100) NOT NULL,
  `anrede` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `fon` varchar(20) NOT NULL,
  `img1` varchar(100) NOT NULL,
  `img2` varchar(100) NOT NULL,
  `img3` varchar(100) NOT NULL,
  `reihenfolge` int(11) NOT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `morp_mitarbeiter`
--

INSERT INTO `morp_mitarbeiter` (`mid`, `aid`, `name`, `vorname`, `anrede`, `email`, `fon`, `img1`, `img2`, `img3`, `reihenfolge`) VALUES
(3, 0, 'Müller', 'Herbert', 'Sehr geehrter Herr', 'test', 'aa', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblCompanies`
--

CREATE TABLE IF NOT EXISTS `tblCompanies` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Company ID',
  `pid` int(11) NOT NULL COMMENT 'Project ID',
  `companyName` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Company Name',
  `legalForm` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Legal Form',
  `ranking` float NOT NULL DEFAULT '0' COMMENT 'Ranking of Company',
  `sum` float NOT NULL COMMENT 'Sum',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Store information of companies' AUTO_INCREMENT=89 ;

--
-- Dumping data for table `tblCompanies`
--

INSERT INTO `tblCompanies` (`id`, `pid`, `companyName`, `legalForm`, `ranking`, `sum`) VALUES
(43, 1, 'Mustermann Industries', 'AG', 13.33, 10);

-- --------------------------------------------------------

--
-- Table structure for table `tblMaincatComXref`
--

CREATE TABLE IF NOT EXISTS `tblMaincatComXref` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `mcid` int(11) NOT NULL COMMENT 'Main Category ID',
  `comid` int(11) NOT NULL COMMENT 'Company ID',
  `factor` float NOT NULL COMMENT 'Factor of each main category',
  `sum` float NOT NULL COMMENT 'sum of each main category',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Store information related between Main Category and Company' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tblMaincatComXref`
--

INSERT INTO `tblMaincatComXref` (`id`, `mcid`, `comid`, `factor`, `sum`) VALUES
(1, 2, 43, 0, 0),
(2, 5, 43, 0, 0),
(3, 5, 65, 2, 30),
(4, 1, 65, 5, 20),
(5, 1, 43, 2, 10);

-- --------------------------------------------------------

--
-- Table structure for table `tblMainCategories`
--

CREATE TABLE IF NOT EXISTS `tblMainCategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Main category id',
  `mid` int(11) NOT NULL COMMENT 'Refer to user id',
  `catName` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Category name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Store catego' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tblMainCategories`
--

INSERT INTO `tblMainCategories` (`id`, `mid`, `catName`) VALUES
(1, 1, 'Unternehmen'),
(2, 1, 'Position'),
(3, 1, 'Standort'),
(4, 1, 'Vergütung'),
(5, 1, 'Sonstige Kriterien');

-- --------------------------------------------------------

--
-- Table structure for table `tblMainCategoryValue`
--

CREATE TABLE IF NOT EXISTS `tblMainCategoryValue` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `mcid` int(11) NOT NULL COMMENT 'Main Category ID',
  `pid` int(11) NOT NULL COMMENT 'Project ID',
  `value` float NOT NULL COMMENT 'Value of Main Category belong to a project',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Store value of main category belong to each project' AUTO_INCREMENT=26 ;

--
-- Dumping data for table `tblMainCategoryValue`
--

INSERT INTO `tblMainCategoryValue` (`id`, `mcid`, `pid`, `value`) VALUES
(1, 1, 1, 5),
(2, 2, 1, 4),
(3, 3, 1, 3),
(4, 4, 1, 2),
(5, 5, 1, 1),
(6, 1, 10, 0),
(7, 2, 10, 0),
(8, 3, 10, 0),
(9, 4, 10, 0),
(10, 5, 10, 0),
(11, 1, 11, 0),
(12, 2, 11, 0),
(13, 3, 11, 0),
(14, 4, 11, 1),
(15, 5, 11, 0),
(16, 1, 12, 0),
(17, 2, 12, 0),
(18, 3, 12, 0),
(19, 4, 12, 0),
(20, 5, 12, 0),
(21, 1, 13, 0),
(22, 2, 13, 0),
(23, 3, 13, 0),
(24, 4, 13, 0),
(25, 5, 13, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblProjects`
--

CREATE TABLE IF NOT EXISTS `tblProjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Project ID',
  `mid` int(11) NOT NULL COMMENT 'User ID',
  `projectName` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Project Name',
  `usrDescription` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Project description by user',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Store projects information' AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tblProjects`
--

INSERT INTO `tblProjects` (`id`, `mid`, `projectName`, `usrDescription`) VALUES
(1, 1, 'The 1st project', 'The project was created for testing purpose');

-- --------------------------------------------------------

--
-- Table structure for table `tblSubcatComXref`
--

CREATE TABLE IF NOT EXISTS `tblSubcatComXref` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `scid` int(11) NOT NULL COMMENT 'Sub-category ID',
  `comid` int(11) NOT NULL COMMENT 'Company ID',
  `usrDescription` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Sub-category description by user',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Store information related between Sub Categories and Companies' AUTO_INCREMENT=58 ;

--
-- Dumping data for table `tblSubcatComXref`
--

INSERT INTO `tblSubcatComXref` (`id`, `scid`, `comid`, `usrDescription`) VALUES
(32, 14, 64, 'asdfasdf'),
(33, 8, 43, ''),
(34, 8, 63, 'asdfasdf'),
(35, 10, 63, 'asdfas'),
(36, 10, 64, 'asdfasdf'),
(37, 8, 64, 'asdfasdf'),
(38, 6, 64, 'asdfasdf'),
(39, 6, 43, ''),
(40, 6, 43, ''),
(41, 7, 43, ''),
(42, 7, 43, ''),
(43, 9, 43, ''),
(44, 10, 43, ''),
(45, 21, 43, ''),
(46, 22, 43, ''),
(47, 23, 43, ''),
(48, 24, 43, ''),
(49, 25, 43, ''),
(50, 1, 43, '144 Mio (180 in 2012)'),
(51, 1, 43, '144 Mio (180 in 2012)'),
(52, 2, 43, '24 Mio (D) - (16%)'),
(53, 3, 43, 'Verbindungstechnologie'),
(54, 4, 43, 'Industrie'),
(55, 4, 65, 'asdfasdf'),
(56, 4, 66, 'sdfasfd'),
(57, 5, 43, 'Nische AT');

-- --------------------------------------------------------

--
-- Table structure for table `tblSubCategories`
--

CREATE TABLE IF NOT EXISTS `tblSubCategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Sub-Category ID',
  `pid` int(11) NOT NULL COMMENT 'Project ID',
  `mcid` int(11) NOT NULL COMMENT 'Main Category ID',
  `catName` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Sub-Category Name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Store sub categories information' AUTO_INCREMENT=201 ;

--
-- Dumping data for table `tblSubCategories`
--

INSERT INTO `tblSubCategories` (`id`, `pid`, `mcid`, `catName`) VALUES
(1, 1, 1, 'Umsatz-Größe '),
(2, 1, 1, 'Anzahl Mitarbeiter'),
(3, 1, 1, 'Ergebnis (EBIT)'),
(4, 1, 1, 'Produkte / Marktanteil'),
(5, 1, 1, 'Branche '),
(6, 1, 2, 'Position'),
(7, 1, 2, 'Aufgabengebiet'),
(8, 1, 2, 'Perspektive'),
(9, 1, 2, 'Reportingline'),
(10, 1, 2, 'Handlungspielraum'),
(11, 1, 3, 'Standort'),
(12, 1, 3, 'Bundesland'),
(13, 1, 3, 'Distanz EM'),
(14, 1, 3, 'Fahrzeit EM'),
(15, 1, 3, 'Lebenshaltungskosten'),
(16, 1, 4, 'Grundgehalt'),
(17, 1, 4, 'Variable'),
(18, 1, 4, 'Gehalts-Perspektive'),
(19, 1, 4, 'Altersvorsorge'),
(20, 1, 4, 'Checked Auto'),
(21, 1, 5, 'Chemie zum Vorgesetzten'),
(22, 1, 5, 'Motivation für Branche '),
(23, 1, 5, 'Erscheinungsbild der Fa. '),
(24, 1, 5, 'Innere Motivation für Job'),
(25, 1, 5, 'Mein Marktwert in 5 Jahren ');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
