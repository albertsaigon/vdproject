<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Entry point
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

defined("__AVALID__") or die;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>vogel-detambel.de</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="<?php echo HTTP_ASSETS_PATH ?>/bootstrap/css/bootstrap.min.css">

	<!-- JQuery UI css 
	<link rel="stylesheet" href="<?php echo HTTP_ASSETS_PATH ?>/jqueryui/css/ui-lightness/jquery-ui-1.10.4.custom.min.css" /> -->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
	
	<link rel="stylesheet" href="<?php echo HTTP_ASSETS_PATH ?>/css/style.css" />
	<link rel="stylesheet" href="<?php echo HTTP_ASSETS_PATH ?>/css/reset.css" />
	<link rel="stylesheet" href="<?php echo HTTP_ASSETS_PATH ?>/css/default.css" />

	<script src="<?php echo HTTP_ASSETS_PATH ?>/jquery/jquery-1.11.0.min.js"></script>

	<!-- JQuery UI js -->
	<script src="<?php echo HTTP_ASSETS_PATH ?>/jqueryui/js/jquery-ui-1.10.4.custom.min.js"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="<?php echo HTTP_ASSETS_PATH ?>/bootstrap/js/bootstrap.min.js"></script>
	

	<script type="text/javascript">

	jQuery(document).ready(function(){

		/*jQuery('#dialog').dialog({
			buttons: {
				"Ok": function() {
						$( this ).dialog( "close" );
					},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			}
		});

		jQuery('#slider').slider({ 
			animate: "fast", 
			buttons: [
				{
					text: "OK",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			],
			min: 0, 
			max: 10,
			step: 1,
			 slide: function( event, ui ) {
				jQuery( "#slider_amount" ).val( ui.value );
			}
		 });
		jQuery( "#slider_amount" ).val( $( "#slider" ).slider( "value" ) );
		
	
		jQuery('#btn_print').click(function(){
			// alert('here');
			var html = '<html>' + jQuery('html').html() + '</html>';
			// alert(html);
			jQuery.ajax({
					type: 'POST',
					url: "index.php",
					data: {
						'view'	: 'pdf',
						'html'	: html,
						'task'	: 'exportpdf'
					},
					success : function(data){
					}
				});
		});*/

		jQuery('.del_project').click(function(){

			var itemId = jQuery(this).prop('id');
			jQuery.ajax({
					url: "index.php",
					data: {
						'view'	: 'setitem',
						'id'	: itemId,
						'value'	: '',
						'task'	: 'delete'
					},
					success : function(data){
					}
				});
		});

		jQuery('[data-onload]').each(function(){
			var itemId = jQuery(this).attr('id');
			var self = jQuery(this);

			jQuery.ajax(
				{
					url: "index.php",
					data: {
						'view'	: 'loaditem',
						'id'	: itemId
					},
					success : function(data){
						if (data.trim() != '')
						{
							self.attr('value', data);
							self.attr('task', 'update');
						}
					}
				});
		});
		
		jQuery('.edit').bind('keypress change', function (e){

			if (e.type === 'change') {
			 	var itemId = jQuery(this).attr('id');
			 	var value = jQuery(this).val();
			 	var task = jQuery(this).attr('task');
			 	var target = jQuery(this);
			 	var arrayId = itemId.split('.');

			 	if ((arrayId[0] == 'mc') && (arrayId[1] == 'factor'))
			 	{
			 		var sumId = target.attr("sum");
			 		var sum = jQuery('[id^="' + sumId + '"]:first');

			 		var mcvalId = target.attr("mcval");
			 		var mc = jQuery('[id^="' + mcvalId + '"]:first');

			 		var sumval = sum.val();
			 		var mcval = mc.val();
			 		var factorval = target.val();

			 		sumval = mcval * factorval;

			 		sum.attr("value", sumval);

			 		var comsumId = sum.attr("comsum");
			 		var comsum = jQuery('[id^="' + comsumId + '"]:first');

			 		var comsumval = 0;
		
				    jQuery('[comsum^="' + comsumId + '"]').each(function() {
				        comsumval += Number(jQuery(this).val());
				    });

			 		comsum. attr("value", comsumval);

			 		var comrankId = comsum.attr("comrank");
			 		comrank = jQuery('[id^="' + comrankId + '"]:first');
			 		var comrankval = comrank.val();
			 		comrankval = (comsumval / 75) * 100;
			 		comrankval = Math.round(comrankval * 100) / 100;
			 		comrank.attr("value", comrankval);

			 	}

			 	if ((arrayId[0] == 'mc') && (arrayId[1] == 'value'))
			 	{
			 		var mcval = target.val();
			 		var mcid = target.prop('id');

			 		jQuery('[mcvalue^="' + mcid + '"]').each(function()
			 		{
			 			var sumId = jQuery(this).prop('id');
			 			var factorId = jQuery(this).attr('factor');
			 			var factor = jQuery('[id^="' + factorId + '"]:first');
			 			var factorval = factor.val();

			 			var sumval = jQuery(this).val();

			 			sumval = mcval * factorval;

			 			jQuery(this).attr("value", sumval);

			 			var comsumId = jQuery(this).attr("comsum");
				 		var comsum = jQuery('[id^="' + comsumId + '"]:first');

				 		var comsumval = 0;
			
					    jQuery('[comsum^="' + comsumId + '"]').each(function() {
					        comsumval += Number(jQuery(this).val());
					    });

				 		comsum. attr("value", comsumval);

				 		var comrankId = comsum.attr("comrank");
				 		comrank = jQuery('[id^="' + comrankId + '"]:first');
				 		var comrankval = comrank.val();
				 		comrankval = (comsumval / 75) * 100;
				 		comrankval = Math.round(comrankval * 100) / 100;
				 		comrank.attr("value", comrankval);

				 		jQuery(this).trigger("change");
				 		comsum.trigger("change");
				 		comrank.trigger("change");


			 		});

			 	}

			 	jQuery.ajax({
					url: "index.php",
					data: {
						'view'	: 'setitem',
						'id'	: itemId,
						'value'	: value,
						'task'	: task
					},
					success : function(data){

						if ((arrayId[0] == 'com') && ((arrayId[1] == 'name') || (arrayId[1] == 'legalform')))
						{
							arrayId[2] = data;
							itemId = arrayId.join('.');
							target.prop('id', itemId);
							target.attr('task', 'update');

							var refId = target.attr('ref');
				 			var refTarget = jQuery('[id^="' + refId + '"]:first');
				 			var arrayRefId = refId.split('.');

				 			arrayRefId[2] = data;
				 			refId = arrayRefId.join('.');
				 			target.attr('ref', refId);

							refTarget.prop('id', refId);
							refTarget.attr('ref', itemId);
							refTarget.attr('task', 'update');

							location.reload(true);
						}

						if ((arrayId[0] == 'subcat') && (arrayId[1] == 'desc'))
						{
							target.attr('task', 'update');
						}

						if ((arrayId[0] == 'mc') && (arrayId[1] == 'value'))
						{
							location.reload(true);
						}

						if ((arrayId[0] == 'mc') && (arrayId[1] == 'factor'))
						{
							target.attr('task', 'update');
							var sumId = target.attr("sum");
			 				var sum = jQuery('[id^="' + sumId + '"]:first');

			 				sum.trigger("change");

			 				var comsumId = sum.attr("comsum");
			 				var comsum = jQuery('[id^="' + comsumId + '"]:first');

			 				comsum.trigger("change");

			 				var comrankId = comsum.attr("comrank");
			 				comrank = jQuery('[id^="' + comrankId + '"]:first');

			 				comrank.trigger("change");
						}

						if (arrayId[0] == 'pro')
						{
							location.reload(true);
						}
					}
				});
		 	}
		});
	});
	</script>
</head>
<body>
