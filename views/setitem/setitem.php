<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Entry point
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

defined("__AVALID__") or die;

/**
 * Class for view projects
 *
 * @package     VdProject.frontend
 * @subpackage  Controller
 * @since       1.0
 */

class Viewsetitem
{
	public $tpl;

	public $view;

	/**
	 * Constructor
	 *
	 * @param   string  $template  template params
	 */
	public function __construct($template = '')
	{
		if (!$template)
		{
			$this->tpl = 'default.php';
		}

		$this->view = "setitem";
		$this->prepare();
		$this->display();
	}

	/**
	 * Displate function
	 *
	 * @return void
	 */
	protected function display()
	{
		AViews::render($this);
	}

	/**
	 * Prepare page function
	 *
	 * @return void
	 */
	protected function prepare()
	{
		$this->id 		= ACore::getVar('id', null);
		$this->value 	= ACore::getVar('value', null);
		$this->task 	= ACore::getVar('task', null);

		$model = new AModel;
		$db = $model->getDbo();

		$tmp = explode('.', $this->id);

		if (count($tmp) >= 3)
		{
			$item = new stdClass;
			$item->type = $tmp[0];
			$item->subtype = $tmp[1];
			$item->id = $tmp[2];

			if (isset($tmp[3]))
			{
				if (($item->type == "subcat") && ($item->subtype == "desc"))
				{
					$item->cid = $tmp[3];
				}

				if (($item->type == "mc") && ($item->subtype == "factor"))
				{
					$item->cid = $tmp[3];
				}

				if (($item->type == "mc") && ($item->subtype == "sum"))
				{
					$item->cid = $tmp[3];
				}

				if ($item->type == 'pro')
				{
					$item->mid = $tmp[3];
				}

				$item->pid = $tmp[3];
			}

			$insertId = $model->doTask($item, $this->value, $this->task);

			if ($this->task == 'insert')
			{
				echo $insertId;
			}
		}
	}
}
