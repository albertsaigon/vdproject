<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Entry point
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

defined("__AVALID__") or die;

/**
 * Class for view projects
 *
 * @package     VdProject.frontend
 * @subpackage  Controller
 * @since       1.0
 */

class Viewloaditem
{
	public $tpl;

	public $view;

	/**
	 * Constructor
	 *
	 * @param   string  $template  template params
	 */
	public function __construct($template = '')
	{
		if (!$template)
		{
			$this->tpl = 'default.php';
		}

		$this->view = "project";
		$this->prepare();
		$this->display();
	}

	/**
	 * Displate function
	 *
	 * @return void
	 */
	protected function display()
	{
		AViews::render($this);
	}

	/**
	 * Prepare page function
	 *
	 * @return void
	 */
	protected function prepare()
	{
		$item = explode('.', ACore::getVar('id', null));

		if ($item[0] == 'subcat' && $item[1] == 'desc')
		{
			$table = 'tblSubcatComXref';
			$columns = array('usrDescription');
			$where = array(
				'scid' => $item[2],
				'comid' => $item[3]
				);

			$db = new ADatabase;

			$callback = $db->loadObject($table, $columns, $where);

			echo @$callback->usrDescription;
		}

		if ($item[0] == 'mc' && $item[1] == 'factor')
		{
			$table = 'tblMaincatComXref';
			$columns = array('factor');
			$where = array(
				'mcid' => $item[2],
				'comid' => $item[3]
				);

			$db = new ADatabase;

			$callback = $db->loadObject($table, $columns, $where);

			echo @$callback->factor;
		}

		if ($item[0] == 'mc' && $item[1] == 'sum')
		{
			$table = 'tblMaincatComXref';
			$columns = array('sum');
			$where = array(
				'mcid' => $item[2],
				'comid' => $item[3]
				);

			$db = new ADatabase;

			$callback = $db->loadObject($table, $columns, $where);

			echo @$callback->sum;
		}

		if ($item[0] == 'com' && $item[1] == 'sum')
		{
			$table = 'tblCompanies';
			$columns = array('sum');
			$where = array(
				'id' => $item[2]
				);

			$db = new ADatabase;

			$callback = $db->loadObject($table, $columns, $where);

			echo @$callback->sum;
		}

		if ($item[0] == 'com' && $item[1] == 'rank')
		{
			$table = 'tblCompanies';
			$columns = array('ranking');
			$where = array(
				'id' => $item[2]
				);

			$db = new ADatabase;

			$callback = $db->loadObject($table, $columns, $where);

			echo @$callback->ranking;
		}

		// Prevent plus data, dont run display function
		exit;
	}
}
