<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Entry point
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

defined("__AVALID__") or die;

$data = ACore::getAssigned('view_data', null);

include_once ABSOLUTE_ROOT_PATH . '/assets/html/header.php';
?>

<div class='row'>
	<div class='col-md-2'><?php echo APROJECT_NAME ?></div>
	<div class='col-md-2'><?php echo APROJECT_FUNCTION ?></div>
	<div class='col-md-8'><?php echo APROJECT_DESCRIPTION ?></div>
</div>

<?php
if (count($data->projects))
{
	foreach ($data->projects as $pro)
	{
		?>
<div class="row">
	<div class="col-md-2"><span><input type="text" class='edit' id="pro.name.<?php echo $pro->id . '.' . $pro->mid; ?>" task="update" value="<?php echo $pro->projectName ?>"></span></div>
	<div class="col-md-2"><span><a href="<?php echo $pro->link ?>"><?php echo APROJECT_DETAIL ?></a>&nbsp;</span><span><a class='del_project' id="del.pro.<?php echo $pro->id ?>"><?php echo ADELETE ?></a></span></div>
	<div class="col-md-8"><span><input type="text" class='edit' id="pro.desc.<?php echo $pro->id . '.' . $pro->mid; ?>" task="update" value="<?php echo $pro->usrDescription ?>"></span></div>
</div>
		<?php
	}
}
?>
<div class="row">
	<div class="col-md-2"><span><input type="text" class='edit' id="pro.name.0.<?php echo $data->mid ?>" task="insert" value=""></span></div>
	<div class='col-md-2'></div>
	<div class="col-md-8"><span><input type="text" class='edit' id="pro.desc.0.<?php echo $data->mid ?>" task="update" value=""></span></div>
</div>
<?php
include_once ABSOLUTE_ROOT_PATH . '/assets/html/footer.php';
