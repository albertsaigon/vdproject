<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Entry point
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

defined("__AVALID__") or die;

/**
 * Class for view projects
 *
 * @package     VdProject.frontend
 * @subpackage  Controller
 * @since       1.0
 */

class Viewprojects
{
	public $tpl;

	public $view;

	/**
	 * Constructor
	 *
	 * @param   string  $template  template params
	 */
	public function __construct($template = '')
	{
		if (!$template)
		{
			$this->tpl = 'default.php';
		}

		$this->view = "projects";

		$this->prepare();

		$this->display();
	}

	/**
	 * Displate function
	 *
	 * @return void
	 */
	protected function display()
	{
		AViews::render($this);
	}

	/**
	 * Prepare function
	 *
	 * @return void
	 */
	protected function prepare()
	{
		$db = new ADatabase;

		$this->mid = (int) $_SESSION['vd_mid'];

		// Check if user with mid exist
		$table = 'morp_mitarbeiter';

		$columns = array(
			'mid', 'aid', 'name', 'vorname', 'anrede',
			'email', 'fon', 'img1', 'img2', 'img3', 'reihenfolge');

		$where = array('mid' => $this->mid);

		$usrs = $db->loadObjectList($table, $columns, $where);

		if (!count($usrs))
		{
			exit;
		}

		$table = 'tblProjects';
		$columns = array('id', 'mid', 'projectName', 'usrDescription');
		$this->projects = $db->loadObjectList($table, $columns);

		if (count($this->projects))
		{
			foreach ($this->projects as $pro)
			{
				$pro->link = 'index.php?view=project&pid=' . $pro->id;
			}
		}
	}
}
