<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Entry point
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

defined("__AVALID__") or die;

/**
 * Class for view projects
 *
 * @package     VdProject.frontend
 * @subpackage  Controller
 * @since       1.0
 */

class Viewproject
{
	public $tpl;

	public $view;

	/**
	 * Constructor
	 *
	 * @param   string  $template  template params
	 */
	public function __construct($template = '')
	{
		if (!$template)
		{
			$this->tpl = 'default.php';
		}

		$this->view = "project";

		$task = ACore::getVar('task', '');
		$this->prepare();

		if ($task == 'pdf')
		{
			// $html = $this->prepareHTML($this);

			// $html = $this->getHTMLContent('http://localhost/techs/vdPROJECT/index.php?view=project&pid=1');

			$this->exportPDF();
		}

		$this->display();
	}

	/**
	 * Displate function
	 *
	 * @return void
	 */
	protected function display()
	{
		AViews::render($this);
	}

	/**
	 * Prepare page function
	 *
	 * @return void
	 */
	protected function prepare()
	{
		$pid = ACore::getVar('pid', 0);

		$this->pid = $pid;

		if (!is_numeric($pid) || !($pid > 0))
		{
			echo "<script>window.location='index.php?view=" . DEFAULT_VIEW . "';</script>";
			exit;
		}

		$params = array();

		$ad = new ADatabase;

		$columns 	= array('id', 'mid', 'projectName', 'usrDescription');
		$where		= array('id' => $pid);
		$order		= array('projectName' => 'ASC');

		$this->project 	= $ad->loadObject("tblProjects", $columns, $where, "AND", $order);

		// Load main categories

		$columns	= array(
						'tblMainCategories.id',
						'tblMainCategories.mid',
						'tblMainCategories.catName',
						'tblMainCategoryValue.value');

		$table  	= array(
						'tblMainCategories',
						'tblMainCategoryValue',
						'INNER JOIN',
						'tblMainCategories.id',
						'tblMainCategoryValue.mcid');

		$where		= array('tblMainCategoryValue.pid' => $pid);

		$order		= array('tblMainCategoryValue.value' => 'DESC');

		$this->mainCategories = $ad->loadObjectList($table, $columns, $where, "AND", $order);

		// Load sub-categories

		$sc = array();

		if (count($this->mainCategories))
		{
			foreach ($this->mainCategories as $mc)
			{
				$table 		= 'tblSubCategories';
				$columns	= array('id', 'pid', 'mcid', 'catName');
				$where 		= array(
								'pid' => $pid,
								'mcid' => $mc->id
								);
				$order 		= array('mcid' => 'ASC');

				$sc[$mc->id] = $ad->loadObjectList($table, $columns, $where, "AND", $order);
			}
		}

		$this->sc = $sc;

		$table 		= 'tblSubCategories';
		$columns	= array('id', 'pid', 'mcid', 'catName');
		$where 		= array('pid' => $pid);
		$order 		= array('mcid' => 'ASC');

		$this->subCategories = $ad->loadObjectList($table, $columns, $where, "AND", $order);

		// Load Companies

		$table 		= 'tblCompanies';
		$columns 	= array('id', 'pid', 'companyName', 'legalForm', 'ranking', 'sum');
		$where 		= array('pid' => $pid);
		$order 		= array('id' => 'ASC');

		$this->companies = $ad->loadObjectList($table, $columns, $where, "AND", $order);
	}

	/**
	 * Prepare HTML function
	 *
	 * @param   mixed  $data  data prepared for generated HTML string
	 * 
	 * @return string HTML output page
	 */
	public function prepareHTML($data)
	{
		$html 	= array();

		/*$html[] = '<div class="row print">';
		$html[] = '	<span>';
		$html[] = '		<a class="btn btn-default" id="btn_print" role="button">Export PDF</a>';
		$html[] = '	</span>';
		$html[] = '</div>';
		*/

		// Company section
		$html[] = '<table>';
		$html[] = '<!-- Companies -->';
		$html[] = '	<tr data-row="com.name">';
		$html[] = '		<td class="title main">Name des Unternehmen</td>';
		$html[] = '		<td class="spacer">&nbsp;</td>';

		if (count($data->companies))
		{
			foreach ($data->companies as $com)
			{
				$html[] = '	<td class="title"><input detail="yes" id="com.name.' . $com->id . '.' . $data->pid . '" class="edit" task="update" ref="com.legalform.' . $com->id . '.' . $data->pid . '" value="' . $com->companyName . '" /></td>';
			}
		}

		$html[] = '		<td class="title"><input detail="no" id="com.name.0.' . $data->pid . '" class="edit" task="insert" ref="com.legalform.0.' . $data->pid . '" value="" /></td>';
		$html[] = '	</tr>';
		$html[] = '	<tr data-row="com.legalform">';
		$html[] = '		<td class="title main">Rechtsform</td>';
		$html[] = '		<td class="spacer">&nbsp;</td>';

		if (count($data->companies))
		{
			foreach ($data->companies as $com)
			{
				$html[] = '	<td class="title"><input detail="yes" id="com.legalform.' . $com->id . '.' . $data->pid . '" class="edit" task="update" ref="com.name.' . $com->id . '.' . $data->pid . '" value="' . $com->legalForm . '" /></td>';
			}
		}

		$html[] = '			<td class="title"><input detail="no" id="com.legalform.0.' . $data->pid . '" class="edit" task="insert" ref="com.name.0.' . $data->pid . '" value="" /></td>';
		$html[] = '		</tr>';
		$html[] = '		<tr><td class="spacer">&nbsp;</td></tr>';

		// Ranking section
		$html[] = '<!-- Ranking -->';
		$html[] = '		<tr>';
		$html[] = '			<td class="title ranking">';
		$html[] = '				Ranking';
		$html[] = '			</td>';
		$html[] = '			<td class="spacer">&nbsp;</td>';

		if (count($data->companies))
		{
			foreach ($data->companies as $com)
			{
				$html[] = '	<td>';
				$html[] = '		<input type="text" class="edit ranking" id="com.rank.' . $com->id . '.' . $data->pid . '" data-onload="" value="0" task="update" readonly/>';
				$html[] = '		<input type="text" class="edit ranking txtgreen" id="com.sum.' . $com->id . '.' . $data->pid . '" comrank="com.rank.' . $com->id . '.' . $data->pid . '" data-onload="" value="0" task="update" readonly/>';
				$html[] = '	</td>';
			}
		}

		$html[] = '		</tr>';
		$html[] = '		<tr><td class="spacer">&nbsp;</td></tr>';

		// Categories section
		$html[] = '<!-- Categories -->';

		if (count($data->mainCategories))
		{
			foreach ($data->mainCategories as $mc)
			{
				$html[] = '	<tr data-row="" class="faktorrow">';
				$html[] = '		<td class="main title">' . $mc->catName . '</td>';
				$html[] = '		<td class="spacer"><input type="text" id="mc.value.' . $mc->id . '.' . $data->pid . '" class="edit bggreen nobord myfont" task="update" value="' . $mc->value . '" data-onload=""/></td>';

				if (count($data->companies))
				{
					foreach ($data->companies as $com)
					{
						$html[] = '	<td>';
						$html[] = '		<div class="row">';
						$html[] = '			<div class="col-md-5">';
						$html[] = '				<div class="row">';
						$html[] = '					<div class="col-md-5 faktor">Faktor</div>';
						$html[] = '					<div class="col-md-5 faktor"><input class="edit bggreen txtongreen greenbord" style="width: 30px;" type="text" id="mc.factor.' . $mc->id . '.' . $com->id . '" value="0" task="insert" data-onload="" sum="mc.sum.' . $mc->id . '.' . $com->id . '" mcval="mc.value.' . $mc->id . '.' . $data->pid . '" /></div>';
						$html[] = '				</div>';
						$html[] = '			</div>';
						$html[] = '			<div class="col-md-5">';
						$html[] = '				<div class="row">';
						$html[] = '					<div class="col-md-5 summe">Summe</div>';
						$html[] = '					<div class="col-md-5 summe"><input class="edit bggreen txtongreen" style="width: 30px; border:none;" type="text" id="mc.sum.' . $mc->id . '.' . $com->id . '" value="0" task="update" data-onload="" factor="mc.factor.' . $mc->id . '.' . $com->id . '" comsum="com.sum.' . $com->id . '.' . $data->pid . '" mcvalue="mc.value.' . $mc->id . '.' . $data->pid . '" readonly/></div>';
						$html[] = '				</div>';
						$html[] = '			</div>';
						$html[] = '		</div>';
						$html[] = '	</td>';
					}
				}

				$html[] = '	</tr>';

				if (count($data->sc[$mc->id]))
				{
					foreach ($data->sc[$mc->id] as $sc)
					{
						$html[] = '	<tr data-row="subcat.desc">';
						$html[] = '		<td class="title"><input type="text" id="subcat.title.' . $sc->id . '" class="edit" value="' . $sc->catName . '" task="update" /></td>';
						$html[] = '		<td class="spacer">&nbsp;</td>';

						if (count($data->companies))
						{
							foreach ($data->companies as $com)
							{
								$html[] = '	<td class="title"><input type="text" id="subcat.desc.' . $sc->id . '.' . $com->id . '" class="edit" data-onload="" mtype="subcat" stype="desc" value="" task="insert" /></td>';
							}
						}

						$html[] = '</tr>';
					}
				}
			}
		}

		$html[] = '</table>';

		return implode("\n", $html);
	}

	/**
	 * Export HTML to PDF
	 *
	 * @param   string  $html  HTML string to export
	 *
	 * @return void
	 */
	public function exportPDF($html = '')
	{
		/*$pdf = new APdf;
		$pdf->WriteHTML($html);
		$pdf->Output();
		exit;*/

		// A4 page in portrait for landscape add -L.
		$mpdf = new mPDF('win-1252', 'A4', '', '', 15, 10, 16, 10, 10, 10);

		// $mpdf->SetHeader('|Your Header here|');
		// $mpdf->setFooter('{PAGENO}');// Giving page number to your footer.
		$mpdf->useOnlyCoreFonts = true;
		$mpdf->SetDisplayMode('fullpage');

		// Buffer the following html with PHP so we can store it to a variable later
		ob_start();

		include "http://localhost/techs/vdPROJECT/index.php?view=project&pid=1";

		// This is your php page

		$html = ob_get_contents();
		ob_end_clean();

		// Send the captured HTML from the output buffer to the mPDF class for processing
		$mpdf->WriteHTML($html);

		// $mpdf->SetProtection(array(), 'user', 'password'); uncomment to protect your pdf page with password.
		$mpdf->Output();
		exit;
	}

	/**
	 * Get HTML content
	 *
	 * @param   string  $url  link to page what to get content
	 *
	 * @return  string html content 
	 */
	public function getHTMLContent($url)
	{
		$html = file_get_contents($url);

		return $html;
	}
}
