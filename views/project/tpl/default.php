<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Entry point
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

defined("__AVALID__") or die;

$data = ACore::getAssigned('view_data', null);

include_once ABSOLUTE_ROOT_PATH . '/assets/html/header.php';
?>
<div class="row print">
	<span>
		<a href="index.php?view=project&pid=1&task=pdf" class="btn btn-default" id="btn_print" role="button">Export PDF</a>
	</span>	
</div>

<table>

	<!-- Companies -->
	<tr data-row="com.name">		
		<td class="title main">Name des Unternehmen</td>
		<td class="spacer">&nbsp;</td>

		<?php
		if (count($data->companies))
		{
			foreach ($data->companies as $com)
			{
				?>
	
			<td class="title"><input detail="yes" id="com.name.<?php echo $com->id . '.' . $data->pid ?>" class="edit" task="update" ref="com.legalform.<?php echo $com->id . '.' . $data->pid ?>" value="<?php echo $com->companyName ?>" /></td>

				<?php
			}
		}
		?>

		<td class="title"><input detail="no" id="com.name.0.<?php echo  $data->pid ?>" class="edit" task="insert" ref="com.legalform.0.<?php echo $data->pid ?>" value="" /></td>

	</tr>
	<tr data-row="com.legalform">
		<td class="title main">Rechtsform</td>
		<td class="spacer">&nbsp;</td>
		<?php
		if (count($data->companies))
		{
			foreach ($data->companies as $com)
			{
				?>
	
		<td class="title"><input detail="yes" id="com.legalform.<?php echo $com->id . '.' . $data->pid ?>" class="edit" task="update" ref="com.name.<?php echo $com->id . '.' . $data->pid ?>" value="<?php echo $com->legalForm ?>" /></td>

				<?php
			}
		}
		?>
		<td class="title"><input detail="no" id="com.legalform.0.<?php echo  $data->pid ?>" class="edit" task="insert" ref="com.name.0.<?php echo $data->pid ?>" value="" /></td>
	</tr>

	<tr><td class="spacer">&nbsp;</td></tr>

	<!-- Ranking -->
	
	<tr>
		<td class="title ranking">
			Ranking
		</td>
		<td class="spacer">&nbsp;</td>
		<?php
		if (count($data->companies))
		{
			foreach ($data->companies as $com)
			{
				?>
		<td>
			<input type="text" class="edit ranking" id="com.rank.<?php echo $com->id . '.' . $data->pid ?>" data-onload="" value="0" task="update" readonly/>
			<input type="text" class="edit ranking txtgreen" id="com.sum.<?php echo $com->id . '.' . $data->pid ?>" comrank="com.rank.<?php echo $com->id . '.' . $data->pid ?>" data-onload="" value="0" task="update" readonly/>
		</td>
				<?php
			}
		}
		?>
	</tr>
	<tr><td class="spacer">&nbsp;</td></tr>

	<!-- Categories -->
	<?php
	if (count($data->mainCategories))
	{
		foreach ($data->mainCategories as $mc)
		{
			?>
	<tr data-row="" class="faktorrow">
		<td class="main title"><?php echo $mc->catName ?></td>
		<td class="spacer"><input type="text" id="mc.value.<?php echo $mc->id . '.' . $data->pid; ?>" class="edit bggreen nobord myfont" task="update" value="<?php echo $mc->value ?>" data-onload=""/></td>
		<?php
			if (count($data->companies))
			{
				foreach ($data->companies as $com)
				{
					?>
		<td>
			<div class="row">
				<div class="col-md-5">
					<div class="row">
						<div class="col-md-5 faktor">Faktor</div>
						<div class="col-md-5 faktor"><input class="edit bggreen txtongreen greenbord" style="width: 30px;" type="text" id="mc.factor.<?php echo $mc->id . '.' . $com->id ?>" value="0" task="insert" data-onload="" sum="mc.sum.<?php echo $mc->id . '.' . $com->id ?>" mcval="mc.value.<?php echo $mc->id . '.' . $data->pid; ?>" /></div>
					</div>
				</div>
				<div class="col-md-5">
					<div class="row">
						<div class="col-md-5 summe">Summe</div>
						<div class="col-md-5 summe"><input class="edit bggreen txtongreen" style="width: 30px; border:none;" type="text" id="mc.sum.<?php echo $mc->id . '.' . $com->id ?>" value="0" task="update" data-onload="" factor="mc.factor.<?php echo $mc->id . '.' . $com->id ?>" comsum="com.sum.<?php echo $com->id . '.' . $data->pid ?>" mcvalue="mc.value.<?php echo $mc->id . '.' . $data->pid; ?>" readonly/></div>
					</div>
				</div>
			</div>
		</td>
					<?php
				}
			}
		?>
	</tr>

			<?php
			if (count($data->sc[$mc->id]))
			{
				foreach ($data->sc[$mc->id] as $sc)
				{
					?>
	<tr data-row="subcat.desc">
		<td class="title"><input type="text" id="subcat.title.<?php echo $sc->id ?>" class="edit" value="<?php echo $sc->catName ?>" task="update" /></td>
		<td class="spacer">&nbsp;</td>
		<?php
			if (count($data->companies))
			{
				foreach ($data->companies as $com)
				{
					?>
		<td class="title"><input type="text" id="subcat.desc.<?php echo $sc->id . '.' . $com->id ?>" class="edit" data-onload="" mtype="subcat" stype="desc" value="" task="insert" /></td>
					<?php
				}
			}
		?>
	</tr>

					<?php
				}
			}
		}
	}
	?>

</table>

<!--<div id='dialog' class='vd_dialog' style="display:none">
	<p>
		<label for="slider_amount">Number:</label>
		<input type="text" id="slider_amount" style="border:0; color:#f6931f; font-weight:bold;">
	</p>
	<div id='slider' class='vd_slider'></div>
</div>-->

<?php
include_once ABSOLUTE_ROOT_PATH . '/assets/html/footer.php';
