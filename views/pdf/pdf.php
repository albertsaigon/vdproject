<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Entry point
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

defined("__AVALID__") or die;

/**
 * Class for view projects
 *
 * @package     VdProject.frontend
 * @subpackage  Controller
 * @since       1.0
 */

class Viewpdf
{
	public $tpl;

	public $view;

	/**
	 * Constructor
	 *
	 * @param   string  $template  template params
	 */
	public function __construct($template = '')
	{
		if (!$template)
		{
			$this->tpl = 'default.php';
		}

		$this->view = "pdf";
		$this->prepare();
		$this->display();
	}

	/**
	 * Displate function
	 *
	 * @return void
	 */
	protected function display()
	{
		AViews::render($this);
	}

	/**
	 * Prepare page function
	 *
	 * @return void
	 */
	protected function prepare()
	{
		$html = ACore::getVar('html', '');
		$pdf = new APdf;
		$pdf->WriteHTML($html);
		$pdf->Output();
		exit;
	}
}
