<?php
/**
 * @package     VdPROJECT.frontend
 * @subpackage  Controller
 *
 * @copyright   Copyright (C) 2014 http://www.vogel-detambel.de. All rights reserved.
 * @license     Some license; see license at http://www.vogel-detambel.de.
 */

defined("__AVALID__") or die;

/**
 * Class controller
 *
 * @package     VdProject.frontend
 * @subpackage  Controller
 * @since       1.0
 */
class AController
{
	/**
	 * The component title to display in the topbar layout (if using it).
	 * It can be html.
	 *
	 * @var string default page
	 */
	protected $defaultPage;

	/**
	 * Constructor
	 *
	 * @param   string  $page  A page you want to display
	 */
	public function __construct($page = '')
	{
		if (!trim($page))
		{
			$this->defaultPage = DEFAULT_VIEW;
		}
		else
		{
			$this->defaultPage = $page;
		}

		$this->display();
	}

	/**
	 * Display a view
	 *
	 * @return void
	 */
	protected function display()
	{
		if (!$this->defaultPage)
		{
			exit;
		}

		$view = ACore::getInstance($this->defaultPage, 'views');
	}
}
